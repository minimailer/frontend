class Routes {
    static uriTemplate(strings, ...keys) {
        return (function(...values) {
            let dict = values[values.length - 1] || {};
            let result = [strings[0]];
            keys.forEach(function(key, i) {
                let value = Number.isInteger(key) ? values[key] : dict[key];
                result.push(value, strings[i + 1]);
            });
            return result.join('');
        });
    }

    static emailsRoutes = {
        getEmails: { method: 'get', uri: 'emails' },
        saveDraftEmail: { method: 'put', uri: 'emails/draft' },
        submitDraftEmail: { method: 'post', uri: 'emails/draft' },
        getAttachments: { method: 'get', uri: this.uriTemplate`emails/${'emailId'}/attachments` },
        getDraft: { method: 'get', uri: 'emails/draft' }
    }

    static attachmentRoutes = {
        sendAttachment: { method: 'post', uri: 'attachments' },
        removeAttachment: { method: 'delete', uri: this.uriTemplate`attachments/${'fileId'}` }
    }
}

export let routes = Routes;