import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@mdi/font/css/materialdesignicons.css'

// Buefy
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy)

// Vuelidate
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

let filter = function(text, length, clamp = '...') {
  return text && text.length > length ? text.slice(0, length) + clamp : text;
};

Vue.filter('truncate', filter);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
