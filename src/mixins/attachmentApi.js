import axios from "axios";
import { routes } from '@/utils/routes'

export default {
    methods: {
        async sendAttachment(emailId, file) {
            let formData = new FormData();
            formData.append("attachment", file);
            formData.append("email_id", emailId);

            return await axios({
                method: routes.attachmentRoutes.sendAttachment.method,
                url: `${process.env.VUE_APP_API_URL}${routes.attachmentRoutes.sendAttachment.uri}`,
                data: formData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(response => {
                return response.data.data
            });
        },
        async removeAttachment(fileId) {
            await axios({
                method: routes.attachmentRoutes.removeAttachment.method,
                url: `${process.env.VUE_APP_API_URL}${routes.attachmentRoutes.removeAttachment.uri({ fileId: fileId })}`,
            });
        }
    }
}