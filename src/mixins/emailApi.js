import axios from "axios";
import * as qs from "qs";
import { ToastProgrammatic as Toast } from 'buefy'
import { routes } from '@/utils/routes'

export default {
    methods: {
        async getEmails(page, filter) {
            return await axios({
                method: routes.emailsRoutes.getEmails.method,
                url: `${process.env.VUE_APP_API_URL}${routes.emailsRoutes.getEmails.uri}`,
                params: { page: page, filter: filter },
                paramsSerializer: (params) => {
                    return qs.stringify(params)
                }
            }).then(response => {
                return response.data;
            }).catch(err => {
                Toast.open({
                    message: err.message,
                    duration: 5000,
                    type: 'is-danger',
                    position: "is-top"
                })
            });
        },
        async saveDraftEmail(data) {
            await axios({
                method: routes.emailsRoutes.saveDraftEmail.method,
                url: `${process.env.VUE_APP_API_URL}${routes.emailsRoutes.saveDraftEmail.uri}`,
                data: data
            }).then(() => {
                Toast.open({
                    message: 'Email was saved',
                    duration: 5000,
                    type: 'is-success',
                    position: "is-top"
                })
            }).catch(err => {
                Toast.open({
                    message: err.message,
                    duration: 5000,
                    type: 'is-danger',
                    position: "is-top"
                })
            })
        },
        async submitDraftEmail(data) {
            await axios({
                method: routes.emailsRoutes.submitDraftEmail.method,
                url: `${process.env.VUE_APP_API_URL}${routes.emailsRoutes.submitDraftEmail.uri}`,
                data: data
            }).then(() => {
                Toast.open({
                    message: 'Email was submitted',
                    duration: 5000,
                    type: 'is-success',
                    position: "is-top"
                })
            }).catch(err => {
                Toast.open({
                    message: err.message,
                    duration: 5000,
                    type: 'is-danger',
                    position: "is-top"
                })
            })
        },
        async getDraftEmail() {
            return await axios({
                method: routes.emailsRoutes.getDraft.method,
                url: `${process.env.VUE_APP_API_URL}${routes.emailsRoutes.getDraft.uri}`
            }).then(response => {
                return response.data
            }).catch(err => {
                Toast.open({
                    message: err.message,
                    duration: 5000,
                    type: 'is-danger',
                    position: "is-top"
                })
            })
        },
        async getEmailAttachments(emailId) {
            return await axios({
                method: routes.emailsRoutes.getAttachments.method,
                url: `${process.env.VUE_APP_API_URL}${routes.emailsRoutes.getAttachments.uri({ emailId: emailId })}`,
            }).then(response => {
                return response.data.data
            })
        }
    }
}