import Vue from 'vue'
import Vuex from 'vuex'
import emails from './modules/emails'
import draftEmail from './modules/draftEmail'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        emails,
        draftEmail
    },
})