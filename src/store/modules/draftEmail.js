import emailApi from "@/mixins/emailApi";

const state = () => ({
    emailData: {
        id: null,
        from: "",
        to: "",
        subject: "",
        textContent: "",
        htmlContent: "",
        status: "Draft",
        createdAt: "",
        updatedAt: "",
        attachments: []
    },
    isLoading: false
})

const getters = {

}

const mutations = {
    setEmailData(state, payload) {
        state.emailData = Object.assign({}, state.emailData, payload)
    },
    setAttachments(state, payload) {
        state.emailData = Object.assign({}, state.emailData, { attachments: payload })
    },
    setIsLoading(state, payload) {
      state.isLoading = payload
    }
}

const actions = {
    async getDraftEmail({commit}) {
        commit('setIsLoading', true);
        let draftEmail = await emailApi.methods.getDraftEmail();
        commit('setEmailData', draftEmail.data);
        commit('setIsLoading', false);
    },
    async getAttachments({commit, state}) {
        let attachments = await emailApi.methods.getEmailAttachments(state.emailData.id);
        commit('setAttachments', attachments.data)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
