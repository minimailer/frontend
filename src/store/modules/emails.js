import emailApi from "@/mixins/emailApi";

const state = () => ({
    isLoading: false,
    list: [],
    pagination: {
        total: 0,
        current: 1,
        perPage: 1
    },
    filter: {
        from: null,
        to: null,
        subject: null,
        status: null,
    }
})

const getters = {

}

const mutations = {
    setList(state, payload) {
        state.list = payload
    },
    setMeta(state, payload) {
      state.pagination.total = payload.total
      state.pagination.current = payload.current_page
      state.pagination.perPage = payload.per_page
    },
    setIsLoading(state, payload) {
        state.isLoading = payload
    },
    setCurrentPage(state, payload) {
        state.pagination.current = payload
    },
    setFilter(state, payload) {
        state.filter = payload
    },
    setFilterFrom(state, payload) {
        state.filter = {...state.filter, from: payload}
    },
    setFilterTo(state, payload) {
        state.filter = {...state.filter, to: payload}
    },
    setFilterSubject(state, payload) {
        state.filter = {...state.filter, subject: payload}
    },
    setFilterStatus(state, payload) {
        state.filter = {...state.filter, status: payload}
    }
}

const actions = {
    getEmails({commit, state}) {
        commit('setIsLoading', true)
        emailApi.methods.getEmails(state.pagination.current, state.filter)
            .then(response => {
                commit('setList', response.data)
                commit('setMeta', response.meta)
                commit('setIsLoading', false)
            });
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
