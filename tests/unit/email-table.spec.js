import {createLocalVue, mount} from '@vue/test-utils'
import Vuex from 'vuex'
import Buefy from 'buefy'
import EmailsTable from '@/components/EmailsTable.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Buefy)

const emailsListData = [
  {
    "id": 2,
    "from": "midway95@mail.ru",
    "to": "vixmontreal@gmail.com",
    "subject": "Privet",
    "textContent": "Some text",
    "htmlContent": null,
    "status": "Posted",
    "createdAt": "2021-01-16T23:00:39.000000Z",
    "updatedAt": "2021-01-16T23:00:39.000000Z",
    "attachments": []
  }
]

const paginationData = {
  total: emailsListData.length,
  current: 1,
  perPage: 1
}

const filterData = {
  from: null,
  to: null,
  subject: null,
  status: null,
}

describe('EmailsTable.vue', () => {
  let state
  let store

  beforeEach(() => {
    state = {
      emailsList: emailsListData,
      pagination: paginationData,
      isLoading: false,
      filter: filterData
    }

    store = new Vuex.Store({
      modules: {
        emails: {
          state
        }
      }
    })
  })

  it('check "<div class="content_container">" exists', () => {
    const wrapper = mount(EmailsTable, { store, localVue })
    expect(wrapper.html()).toContain('<div class="content_container">')
  })
})
