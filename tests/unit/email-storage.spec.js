import storage from '../../src/store/modules/emails'

test('test "setIsLoading" mutation', () => {
  const state = {
    isLoading: true
  }
  storage.mutations.setIsLoading(state, false);
  expect(state.isLoading).toBe(false)
})

// same for every mutation