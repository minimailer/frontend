import {createLocalVue, shallowMount} from '@vue/test-utils'
import Vuex from 'vuex'
import Buefy from 'buefy'
import CreateEmail from '@/views/CreateEmail.vue'
import emailApi from "../mixins/emailApi";
import attachmentApi from "../mixins/attachmentApi";

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Buefy)

const draftEmailData = {
  id: null,
  from: "",
  to: "",
  subject: "",
  textContent: "",
  htmlContent: "",
  status: "Draft",
  createdAt: "",
  updatedAt: "",
  attachments: []
}
const isDraftLoadingData = false

describe('CreateEmail.vue', () => {
  let state
  let actions
  let store

  beforeEach(() => {
    state = {
      emailData: draftEmailData,
      isLoading: isDraftLoadingData
    }

    actions = {
      getDraftEmail: jest.fn(),
      getDraftAttachments: jest.fn()
    }

    store = new Vuex.Store({
      modules: {
        draftEmail: {
          state,
          actions,
          namespaced: true
        }
      }
    })
  })

  it('check "sendFileToBackend" method', async () => {
    const wrapper = shallowMount(CreateEmail, { store, localVue, mixins: [emailApi, attachmentApi] })
    wrapper.vm.uploadingToBackend = true
    wrapper.vm.dropFiles = [1,2,3]
    await wrapper.vm.sendFileToBackend();
    expect(wrapper.vm.uploadingToBackend).toBe(false)
    expect(wrapper.vm.dropFiles).toStrictEqual([])
  })
})